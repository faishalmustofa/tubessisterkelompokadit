## Penjelasan Singkat
Aplikasi Smart House ini menggunakan protokol MQTT yang merupakan salah satu protokol Publish-Subscribe pada Indirect Communication. Aplikasi ini dapat menyalakan dan mematikan lampu melalui aplikasi web yang dapat diakses di browser PC atau smartphone.

## Daftar Kelompok
* Aditya Eka Bagaskara
* Aditya Standley Christianto
* Ahmad Rizky Prayogi
* Rafli Faishal Mustofa

## Detail Broker
1. Host : 149.56.36.129
1. Port : 1883
1. Websocket Port : 18083

## Cara Kerja
1. Mikrokontroler men-subscribe ke broker dengan topik 'lampu'
1. Websocket men-subscribe ke broker dengan topik 'status'
1. Websocket men-publish pesan perintah ke broker dengan topik 'lampu'
1. Broker meneruskan pesan dari websocket ke mikrokontroler
1. Mikrokontroler memproses pesan yang diterima
1. Lampu akan dinyalakan atau dimatikan sesuai pesan yang diterima

## Fitur Baru
### Mikrokontroler Publish Status (Aditya Eka)
Setiap mikrokontroler selesai menjalankan perintah untuk menyalakan atau mematikan lampu, mikrokontroler akan mencatat status dari semua lampu. Misalnya lampu 1 sedang menyala atau lampu 2 sedang mati. Status-status tersebut kemudian akan di-publish ke broker dengan topik 'status'.

### Websocket Random lampu yang dinyalakan dan dimatikan(Ahmad Rizky Prayogi)
Disini akan dipanggil fungsi random dari Javascript, lalu dari hasil itu disimpan nilainya dan lampu yang menyala tergantung nilai yang keluar dari fungsi random tersebut, dan disini juga bisa mematikan lampu secara random cara kerjanya sama seperti saat menyalakan hanya button yang diklik pada bagian off

### Websocket Custom Message (Rafli Faishal Mustofa)
Custom Message akan menerima input dari user untuk menyalakan dan mematikan lampu. Misalkan, pesan status #on_1 untuk menyalakan lampu 1 dan #off_1 untuk mematikan Lampu 1. Costum message juga bisa menerima pesan lainnya untuk memberi tahu ke web socket lain.

### Websocket Subscribe Status (Aditya Standley C)
Websocket akan mengupdate status sesuai keadaan lampu, di web akan ditampilkan lampu mana saja yang mati dan lampu mana saja yang menyala dengan mensubscribe status yang sudah di publish
oleh mikrokontroler